﻿using System.ComponentModel.DataAnnotations;
using System;

namespace FileExchange.Models
{
    public class Text
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string SavedText { get; set; }
        public string OwnerEmail { get; set; }
        public int Views { get; set; }
        public bool DeleteOnView { get; set; }
        [DataType(DataType.Date)]
        public DateTime CreationDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime expirationDate { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace FileExchange.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Please enter your email address.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter your password.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}

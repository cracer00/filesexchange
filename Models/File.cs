﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FileExchange.Models
{
    public class File
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string OwnerEmail { get; set; }
        public int DownloadedTimes { get; set; }
        public bool DeleteOnDownload { get; set; }
        [DataType(DataType.Date)]
        public DateTime CreationDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime expirationDate { get; set; }
    }
}

﻿using FileExchange.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.IO;
using System.Threading.Tasks;

namespace FileExchange.Controllers
{
    public class TextsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public TextsController(IWebHostEnvironment webHostEnvironment, ApplicationDbContext context)
        {
            _webHostEnvironment = webHostEnvironment;
            _context = context;
        }

        [Route("texts/")]
        [HttpGet]
        [Authorize]
        public IActionResult Texts()
        {
            string htmlContent = "";
            foreach (Models.Text item in _context.Texts)
            {
                // If text belongs to a user - add a link to the page
                if (item.OwnerEmail == HttpContext.User.Identity.Name)
                {
                    htmlContent += $@"<a href='/texts/delete/{item.Id}' class='btn btn-primary btn-sm mr-2' role='button' aria-disabled='true'>Delete</a><a href='/texts/{item.Id}'>{item.Name}</a></br>";
                }
            }
            // Passing htmlContent to the view
            ViewBag.TextsCode = htmlContent;
            return View();
        }

        [Route("texts/add")]
        [HttpGet]
        [Authorize]
        public IActionResult Add()
        {
            return View();
        }

        [Route("texts/adding")]
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> AddingAsync(Text text)
        {
            // Id generation
            string textId = Guid.NewGuid().ToString("N");
            string name = text.Name;
            string savedText = text.SavedText;
            // Adding text to DB
            _context.Texts.Add(new Models.Text
            {
                Id = textId,
                Name = text.Name.Replace("'", "''"),  // Shield the single quote
                SavedText = text.SavedText.Replace("'", "''"),  // Shield the single quote
                Views = 0,
                OwnerEmail = HttpContext.User.Identity.Name,
                CreationDate = DateTime.Now,
                DeleteOnView = text.DeleteOnView
            });
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", "Texts");
        }

        [Route("texts/{text_id}")]
        [HttpGet]
        public async Task<IActionResult> GetText(string text_id)
        {
            // Looking for a text with ID in the database
            Models.Text currentText = await _context.Texts.FirstOrDefaultAsync(t => t.Id == text_id);
            if (currentText != null)
            {
                // Text deletion after the first view parameter handler 
                if (currentText.DeleteOnView == true)
                {
                    _context.Texts.Remove(currentText);
                }
                else
                {
                    currentText.Views = ++currentText.Views;
                }
                await _context.SaveChangesAsync();
            }
            else
            {
                return RedirectToAction("0300", "Error");
            }
            // Return ViewBag with Text object
            ViewBag.Message = currentText;
            return View();
        }

        [Route("texts/delete/{text_id}")]
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> DeleteTextAsync(string text_id)
        {
            // Looking for a text with ID in the database
            Models.Text currentText = await _context.Texts.FirstOrDefaultAsync(t => t.Id == text_id);
            if (currentText != null)
            {
                // Find out if the text belongs to the user with OwnerEmail
                if (currentText.OwnerEmail == HttpContext.User.Identity.Name)
                {
                    _context.Texts.Remove(currentText);
                    await _context.SaveChangesAsync();
                }
                else return RedirectToAction("0302", "Error");
            }
            else
            {
                return RedirectToAction("0300", "Error");
            }
            return RedirectToAction("Index", "Texts");
        }
    }
}

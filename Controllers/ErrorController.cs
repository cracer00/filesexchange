﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;

namespace FileExchange.Controllers
{
    public class ErrorsController : Controller
    {
        [HttpGet("Error/500")]
        public IActionResult Error500()
        {
            var exceptionFeature = HttpContext.Features.Get<IExceptionHandlerPathFeature>();

            if (exceptionFeature != null)
            {
                ViewBag.ErrorMessage = exceptionFeature.Error.Message;
                ViewBag.RouteOfException = exceptionFeature.Path;
            }

            return View();
        }

        [HttpGet("Error/{StatusCode}")]
        public IActionResult Error(int statusCode)
        {
            switch (statusCode)
            {
                case 404:
                    ViewBag.ErrorMessage = "Page not found.";
                    break;

                case 500:
                    ViewBag.ErrorMessage = "Internal server error.";
                    break;

                case 0100: 
                    ViewBag.ErrorMessage = "Username or email already taken by another user.";
                    break;

                case 0200:
                    ViewBag.ErrorMessage = "No such file or directory.";
                    break;

                case 0201:
                    ViewBag.ErrorMessage = "There is no file for delete.";
                    break;

                case 0202:
                    ViewBag.ErrorMessage = "This file doesn't belong to you.";
                    break;

                case 0300:
                    ViewBag.ErrorMessage = "No such text.";
                    break;

                case 0302:
                    ViewBag.ErrorMessage = "This text doesn't belong to you.";
                    break;

                default:
                    break;
            }

            return View();
        }
    }
}
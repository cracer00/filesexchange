﻿using FileExchange.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace FileExchange.Controllers
{
    public class FileUploadController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly string _uploadDir = "\\upload\\";
        private readonly IWebHostEnvironment _webHostEnvironment;

        public FileUploadController(IWebHostEnvironment webHostEnvironment, ApplicationDbContext context)
        {
            _webHostEnvironment = webHostEnvironment;
            _context = context;
        }

        public class FileUploadApi
        {
            public IFormFile files { get; set; }
        }

        [Route("files/post")]
        [Authorize]
        public async Task<IActionResult> PostFile([FromForm] FileUploadApi objFile, [FromForm] string burnItAfter)
        {
                if (objFile.files.Length > 0)
                {
                    bool checkboxStatus = false;
                    // Get the value of the checkbox
                    if (burnItAfter == "on")
                    {
                        checkboxStatus = true;
                    }
                    string pathToFile = _uploadDir + objFile.files.FileName;
                    // Generate Id
                    string fileId = Guid.NewGuid().ToString("N");
                    _context.Files.Add(new Models.File
                    {
                        Id = fileId,
                        Name = objFile.files.FileName,
                        Path = _uploadDir + fileId,
                        OwnerEmail = HttpContext.User.Identity.Name,
                        DeleteOnDownload = checkboxStatus,
                        DownloadedTimes = 0,
                        CreationDate = DateTime.Now
                    });
                    await _context.SaveChangesAsync();

                    if (!Directory.Exists(_webHostEnvironment.WebRootPath + _uploadDir))
                    {
                        Directory.CreateDirectory(_webHostEnvironment.WebRootPath + _uploadDir);
                    }

                    await using (FileStream fileStream = System.IO.File.Create(_webHostEnvironment.WebRootPath + _uploadDir + fileId))
                    {
                        objFile.files.CopyTo(fileStream);
                        fileStream.Flush();
                    }
                    return RedirectToAction("Index", "Files");
                }
                else return RedirectToAction("0200", "Error");
        }

        [Route("files/upload")]
        [HttpGet]
        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        [Route("files/")]
        [HttpGet]
        [Authorize]
        public IActionResult Files()
        {
            string htmlContent = "";
            foreach (Models.File item in _context.Files)
            {
                // If the file belongs to a user - add a link to the page
                if (item.OwnerEmail == HttpContext.User.Identity.Name)
                {
                    htmlContent += $@"<a href='/files/delete/{item.Id}' class='btn btn-primary btn-sm mr-2' role='button' aria-disabled='true'>Delete</a><a href='/files/{item.Id}'>{item.Name}</a></br>";
                }
            }
            ViewBag.FilesCode = htmlContent;
            return View();
        }

        [Route("files/delete/{file_id}")]
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> DeleteFileAsync(string file_id)
        {
            string filePath = _webHostEnvironment.WebRootPath;
            string fileName = "";
            // Looking for a file by ID in the database
            Models.File currentFile = await _context.Files.FirstOrDefaultAsync(f => f.Id == file_id);
            if (currentFile != null)
            {
                // Find out if the file belongs to the user with OwnerEmail
                if (currentFile.OwnerEmail == HttpContext.User.Identity.Name)
                {
                    filePath += currentFile.Path;
                    fileName = currentFile.Name;
                    _context.Files.Remove(currentFile);
                    // Path to /upload/ folder
                    var fullPath = _webHostEnvironment.WebRootPath + "/upload/" + currentFile.Id;
                    System.Diagnostics.Debug.WriteLine(fullPath);
                    try
                    {
                        if (System.IO.File.Exists(fullPath))
                        {
                            System.IO.File.Delete(fullPath);
                        }
                    }
                    catch
                    {
                        return RedirectToAction("0201", "Error");
                    }
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index", "Files");
                }
                else return RedirectToAction("0202", "Error");
            }
            else
            {
                return RedirectToAction("0200", "Error");
            }
            
        }

        [Route("files/{file_id}")]
        [HttpGet]
        public async Task<IActionResult> GetFile(string file_id)
        {
            string filePath = _webHostEnvironment.WebRootPath;
            // Looking for a file with ID in the database
            Models.File currentFile = await _context.Files.FirstOrDefaultAsync(f => f.Id == file_id);
            string fileName = "";
            if (currentFile != null)
            {
                fileName = currentFile.Name;
                filePath += currentFile.Path;

                // Create a stream to send the file to the user
                FileStream fs = new FileStream(filePath, FileMode.Open);
                var fileExt = Path.GetExtension(currentFile.Name);
                string file_type = "application/" + fileExt;
                // File to send back to user
                var backFile = File(fs, file_type, fileName);

                // File deletion after the first download parameter handler 
                if (currentFile.DeleteOnDownload == true)
                {
                    _context.Files.Remove(currentFile);
                    var fullPath = _webHostEnvironment.ContentRootPath + "/upload/" + currentFile.Id;
                    try
                    {
                        if (System.IO.File.Exists(fullPath))
                        {
                            System.IO.File.Delete(fullPath);
                        }
                    }
                    catch
                    {
                        return RedirectToAction("0201", "Error");
                    }
                }
                else
                {
                    currentFile.DownloadedTimes = ++currentFile.DownloadedTimes;
                }
                await _context.SaveChangesAsync();
                return backFile;
            } else
            {
                return RedirectToAction("0200", "Error");
            }
        }
    }
}
